# Kubernetes Project

> Make sure you have installed kops, terraform, kubectl and the aws cli tools.

## Prerequisites

First configure you cli to communicate with your AWS account

```sh
$ aws configure
AWS Access Key ID [****************ZQ4Q]:
AWS Secret Access Key [****************NgfX]:
Default region name [eu-west-1]:
Default output format [json]:
```

Then create a S3 bucket to store kops configuration

```sh
bucket_name=bucket-kops-store-discourse

aws s3api create-bucket \
--bucket ${bucket_name} \
--region eu-west-1 \
--create-bucket-configuration LocationConstraint=eu-west-1

aws s3api put-bucket-versioning --bucket ${bucket_name} --versioning-configuration Status=Enabled
```

Export some needed environment variables

```sh
# supinfo.maxime-brunet.fr is an existing hosted zone in Route53
export KOPS_CLUSTER_NAME=supinfo.maxime-brunet.fr  
export KOPS_STATE_STORE=s3://${bucket_name}
```

Finally create your terraform file describing the cluster with needed configuration

```sh
kops create cluster \
--master-count=1 \
--node-count=2 \
--node-size=t2.2xlarge \
--zones=eu-west-1a \
--name=${KOPS_CLUSTER_NAME} \
--out=. \
--target=terraform
```

Now you can check the generated files

```sh
terraform plan
```

`If you have terraform version > 0.12, you have to fix errors`

After that:

```sh
terraform apply
```

Below, some commands to check your cluster:

- validate cluster:

```sh
kops validate cluster
```

- list nodes:

```sh
kubectl get nodes --show-labels
```

- ssh to the master:

```sh
ssh -i ~/.ssh/id_rsa admin@api.${KOPS_CLUSTER_NAME}
```

## Helm

Assuming you've intalled helm cli.

```sh
# Create service account for tiller
kubectl -n kube-system create serviceaccount tiller

# Add privilege for the service account
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller

# Install tiller with the service account
helm init --service-account tiller
```

## Utility

> This is a custom charts in order to handle ingress ressources, create entry in Route53, generated https certificat.

```sh
# Update dependency of chart
helm dependency update common/utility
# Install chart
helm install --name utility --set external-dns.domainFilters=${KOPS_CLUSTER_NAME}. common/utility
# Create ClusterIssuer
kubectl apply -f common/letsencrypt-prod.yaml
```

```sh
# Edit cluster to add policie for externalDNS
kops edit cluster ${CLUSTER_NAME}
```

Add the following content in spec parts

```yaml
spec:
  additionalPolicies:
    node: |
      [
        {
          "Effect": "Allow",
          "Action": [
            "route53:ChangeResourceRecordSets"
          ],
          "Resource": [
            "arn:aws:route53:::hostedzone/*"
          ]
        },
        {
          "Effect": "Allow",
          "Action": [
            "route53:ListHostedZones",
            "route53:ListResourceRecordSets"
          ],
          "Resource": [
            "*"
          ]
        }
      ]
```

```sh
# Apply the new configuration into the cluster
kops update cluster ${CLUSTER_NAME} --yes
kops rolling-update cluster
```

## Discourse

```sh
$ kubectl apply -f discourse/service.yaml
service/discourse created

$ kubectl apply -f discourse/discourse-data-persistentvolumeclaim.yaml
persistentvolumeclaim/discourse-data created

$ kubectl apply -f discourse/postgresql-data-persistentvolumeclaim.yaml
persistentvolumeclaim/postgresql-data created

$ kubectl apply -f discourse/redis-data-persistentvolumeclaim.yaml
persistentvolumeclaim/redis-data created

$ kubectl apply -f discourse/sidekiq-data-persistentvolumeclaim.yaml
persistentvolumeclaim/sidekiq-data created

$ kubectl apply -f discourse/postgresql-deployment.yaml
deployment.extensions/postgresql created

$ kubectl apply -f discourse/redis-deployment.yaml
deployment.extensions/redis created

$ kubectl apply -f discourse/discourse-deployment.yaml
deployment.extensions/discourse created

$ kubectl apply -f discourse/sidekiq-deployment.yaml
deployment.extensions/sidekiq created

$ kubectl apply -f discourse/ingress.yaml
ingress.extensions/ingress-discourse created
ingress.extensions/ingress-kibana created
ingress.extensions/ingress-grafana created
```

## ELK

### Creation of the namespace logs

```sh
kubectl apply -f elk/ns.yaml
```

### Installation of ELK stack

```sh
helm install --namespace logs --name elastic-stack -f elk/values.yaml --version 1.6.0 stable/elastic-stack
```

### Connect to your Kibana interface

```sh
export POD_NAME=$(kubectl get pods --namespace logs -l "app=elastic-stack,release=elastic-stack" -o jsonpath="{.items[0].metadata.name}")

kubectl port-forward --namespace logs $POD_NAME 5601
```

Visit <http://127.0.0.1:5601> to use Kibana

Or you can just visit <https://kibana.${KOPS_CLUSTER_NAME}>

## Monitoring

### Creation of the namespace monitoring

```sh
kubectl apply -f monitoring/ns.yaml
```

### Prometheus

#### Installation via helm

```sh
helm install --namespace monitoring --name prometheus stable/prometheus
```

#### Access to Prometheus

```sh
export POD_NAME=$(kubectl get pods --namespace monitoring  -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")

kubectl --namespace monitoring  port-forward $POD_NAME 9090
```

### Grafana

#### Installation

```sh
helm install --namespace monitoring --name grafana stable/grafana
```

#### Access to Grafana

```sh
export POD_NAME=$(kubectl get pods --namespace monitoring  -l "app=grafana" -o jsonpath="{.items[0].metadata.name}")

kubectl port-forward $POD_NAME 3000
```

Visit <http://127.0.0.1:3000> to use Grafana

> Don't forget to let the port-forward for the prometheus.

Or you can just visit <https://grafana.${KOPS_CLUSTER_NAME}>

#### Get admin password for Grafana

```sh
kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

#### Configuration

- Add a new datasources of type prometheus
- Choose:
  - url serveur: <http://prometheus-server>
  - access mode: server

Useful dashboard for kubernetes in Grafana: <https://grafana.com/dashboards/6417>

## Delete cluster

```sh
kubectl apply -f discourse/ingress.yaml
kubectl apply -f discourse/service.yaml
kubectl apply -f discourse/postgresql-deployment.yaml
kubectl apply -f discourse/redis-deployment.yaml
kubectl apply -f discourse/discourse-deployment.yaml
kubectl apply -f discourse/sidekiq-deployment.yaml
kubectl apply -f discourse/discourse-data-persistentvolumeclaim.yaml
kubectl apply -f discourse/postgresql-data-persistentvolumeclaim.yaml
kubectl apply -f discourse/redis-data-persistentvolumeclaim.yaml
kubectl apply -f discourse/sidekiq-data-persistentvolumeclaim.yaml

helm del --purge prometheus
helm del --purge grafana
helm del --purge elastic-stack
helm del --purge utility

terraform destroy

kops delete cluster --name ${KOPS_CLUSTER_NAME} --yes

connect to your AWS account and delete the bucket
```

## Improvement List

- Separate postgres and redis configuration with secret and configmap.
- Terraform bucket creation
- Automate Installation
- Automate Destruction

## Useful links

- <https://aws.amazon.com/fr/route53/>
- <https://aws.amazon.com/fr/console/>
- <https://kubernetes.io/>
- <https://github.com/kubernetes/kompose>
- <https://github.com/kubernetes-incubator/external-dns>
- <https://helm.sh/>
- <https://www.terraform.io/>
- <https://github.com/kubernetes/kops>
- <https://github.com/jetstack/cert-manager>
- <https://github.com/kubernetes/ingress-nginx>
